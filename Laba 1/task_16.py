import numpy as np


A = np.random.randint(10, size=4).reshape(-1, 2)
B = np.random.randint(10, size=6).reshape(-1, 2)
AB_x = np.reshape(np.meshgrid(A[:, 0], B[:, 0]), newshape=(2, -1))
AB_y = np.reshape(np.meshgrid(A[:, 1], B[:, 1]), newshape=(2, -1))
distances = ((AB_x[0]-AB_x[1])**2+(AB_y[0]-AB_y[1])**2)**(1/2)
m = np.argmax(distances)


def show():
    print('A: ', "\n", A)
    print('B: ', "\n", B)
    print('Max: ', [AB_x[..., m][0], AB_y[..., m][0]], [AB_x[..., m][1], AB_y[..., m][1]])


