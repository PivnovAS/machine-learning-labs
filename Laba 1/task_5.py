import numpy as np
import matplotlib.pyplot as plt


def f(X):
    return 1/X


x = np.linspace(0.1, 10, 10000)


def show():
    print('Task 5:')
    plt.scatter(x, f(x), s=1)
    plt.show()

