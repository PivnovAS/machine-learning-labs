import numpy as np
import matplotlib.pyplot as plt


def r(S):
    return a*np.cos(k*S+y)


def d_x(X):
    return r(X)*np.cos(X)


def d_y(Y):
    return r(Y)*np.sin(Y)


x = np.linspace(0, 2*np.pi, 1000)
a = 2
k = 4
y = 0


def show():
    print('Task 11:')
    plt.plot(d_x(x), d_y(x))
    plt.show()


