import numpy as np


data = np.genfromtxt("Data8.csv", delimiter=",")
X = data[:, [0, 1]]
y = data[:, 2]

yu = np.unique(y)
Min = np.min([len(np.where(y == i)[0]) for i in yu])
Indexes = np.array([np.where(y == i)[0][:Min] for i in yu]).reshape(1, -1)[0]
Indexes = np.random.permutation(Indexes)
ytr = y[Indexes]
Xtr = X[Indexes]


def show():
    print('Task 8:')
    print(Xtr, ytr)



