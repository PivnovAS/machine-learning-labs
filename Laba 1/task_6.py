import numpy as np
import math
import matplotlib.pyplot as plt


def f(X):
    y = np.array([math.log(i) for i in X])
    return y


x = np.linspace(1, 10, 1000)


def show():
    print('Task 6:')
    print(x, f(x))
    plt.scatter(x, f(x), s=1)
    plt.show()

