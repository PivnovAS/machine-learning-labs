import numpy as np
import matplotlib.pyplot as plt


def f(X):
    return np.sin(X) + np.random.normal(0, 0.2, 1000)


x = np.linspace(0, 20, 1000)


def show():
    print('Task 7:')
    plt.scatter(x, f(x), s=1)
    plt.show()

