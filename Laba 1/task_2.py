import numpy as np


def f(X, W, B):
    return np.dot(X, W) + B


x = np.array([2, 4, 2, 7])
w = np.array([3, 5, 7, 3])
b = 5


def show():
    print('Task 2:')
    print('x =', x)
    print('w =', w)
    print('b =', b)
    print('f =', f(x, w, b))


