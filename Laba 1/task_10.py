import numpy as np


def f(X):
    np.random.shuffle(X)
    X = X.reshape(-1, 2)
    return X


N = 10
x = np.arange(1, 2*N+1)


def show():
    print('Task 10:')
    print(f(x))


