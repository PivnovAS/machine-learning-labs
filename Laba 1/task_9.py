import numpy as np
import matplotlib.pyplot as plt


def f(X):
    return np.sin(X) + np.random.normal(0, 0.2, 3000)


def f2(X):
    y = np.sin(X) + np.random.normal(0, 0.2, 3000)
    y2 = y
    for i in range(1, len(y)-1):
        y2[i] = np.mean([y[i-1], y[i], y[i+1]])
    return y2


def f3(X):
    y = np.sin(X) + np.random.normal(0, 0.2, 3000)
    y2 = y
    for i in range(1, len(y)-1):
        y2[i] = np.median([y[i-1], y[i], y[i+1]])
    return y2


x = np.linspace(0, 20, 3000)
y = f(x)


def show():
    print('Task 9:')
    plt.scatter(x, f(x), s=1, c="b")
    plt.scatter(x, f2(x), s=1, c="r")
    plt.scatter(x, f3(x), s=1, c="#B5F1A2")
    plt.show()


