import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


x = sp.linspace(-8, 8, 1000)
y = sp.linspace(-8, 8, 1000)
x, y = sp.meshgrid(x, y)
z = sp.sin((2*x**2+5*y**2)**(1/4))
fig_1 = plt.figure(1)
axes = Axes3D(fig_1)
c = axes.plot_surface(x, y, z, cmap='Blues')

fig_2 = plt.figure(2)
map = fig_2.add_subplot(111)
map.pcolormesh(x, y, z, cmap='Blues')


def show():
    plt.show()

