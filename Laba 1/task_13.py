import numpy as np
import matplotlib.pyplot as plt


x = np.random.normal(loc=0, scale=1000, size=500)
cen = np.mean(x)
var = np.var(x)
fig = plt.figure()
ax = fig.add_subplot(111)
ax.hist(x, bins=50, density=True)

x_f = np.linspace(start=x.min(), stop=x.max(), num=100)
y = 1/((var*2*np.pi)**(1/2))*np.exp(-(x_f-cen)**2/(2*var))


def show():
    plt.plot(x_f, y, c='g')
    plt.show()


