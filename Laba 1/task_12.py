import numpy as np
import matplotlib.pyplot as plt


def f(X):
    return np.sqrt(25 - X*X)


x = np.linspace(-5, 5, 10000)


def show():
    print('Task 12:')
    plt.scatter(x, f(x), s=1)
    plt.show()


