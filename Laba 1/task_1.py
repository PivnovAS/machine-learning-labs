import numpy as np


def f(X):
    return 1/2 * np.sum(np.cos(X)**2)


x = np.array([0, 1, 2])


def show():
    print('Task 1:')
    print('x =', x)
    print('f =', f(x))
