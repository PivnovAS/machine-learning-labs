import scipy as sp
import scipy.optimize as opt
import numpy as np
import pylab
from mpl_toolkits.mplot3d import Axes3D


def Loss(y, t):
    return np.sum((y - t) ** 2)


def funct(Params):
    w1, w2, b = Params
    w = np.array([w1, w2])
    t = np.array([np.dot(x[i], w) + b for i in range(len(y))])
    return Loss(y, t)


data = np.genfromtxt("Data4.csv", delimiter=",")

x = data[:, [0, 1]]
y = data[:, 2]

params = np.array([0]*3)
result = opt.minimize(funct, params)
params = result.x

fig = pylab.figure()
axes = Axes3D(fig)

x_gr, y_gr = sp.meshgrid(x[..., 0:1], x[..., 1])
w1, w2, b = params
z_gr = np.array([w1*x_gr[i]+w2*y_gr[i]+b for i in range(len(y))])
axes.plot_wireframe(x_gr, y_gr, z_gr, color='g')
axes.scatter(x[..., 0:1], x[..., 1], y, color='r')


def show():
    pylab.show()


