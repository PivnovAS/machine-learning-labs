import numpy as np
import scipy.optimize as opt


def Loss(y, t):
    return np.sum((y - t) ** 2)


def funct(Params):
    w1, w2, b = Params
    w = np.array([w1, w2])
    t = np.array([np.dot(x[i], w) + b for i in range(len(y))])
    return Loss(y, t)


data = np.genfromtxt("Data4.csv", delimiter=",")

x = data[:, [0, 1]]
y = data[:, 2]

params = np.array([0]*3)
result = opt.minimize(funct, params)


def show():
    print('Task 4:')
    print('w1 =', result.x[0], 'w2 =', result.x[1], 'b =', result.x[2])



