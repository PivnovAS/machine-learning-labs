from matplotlib import pyplot as plt
import numpy as np
from sklearn.datasets import load_iris
from sklearn import svm

data = load_iris()
features = data.data
feature_names = data.feature_names
target = data.target
target_names = data.target_names


# sepal length - sepal width    0 1
# sepal length - petal length   0 2
# sepal length - petal width    0 3
# sepal width - petal length    1 2
# sepal width - petal width     1 3
# petal length - petal width    2 3

def BuildingDiagram(p1, p2):
    for t in range(3):
        c = '0'
        if t == 0:
            c = 'r'
        elif t == 1:
            c = 'g'
        elif t == 2:
            c = 'b'

        plt.scatter(features[target == t, p1],
                    features[target == t, p2],
                    c=c)
    plt.show()


BuildingDiagram(2, 3)


Indexes = [[0, 1],
           [0, 2],
           [0, 3],
           [1, 2],
           [1, 3],
           [2, 3]]

P = []
CountArray = 150

for i, j in Indexes[:]:
    X = features[:, [i, j]]
    y = data.target

    clf = svm.SVC(kernel='linear', C=1.0)
    clf.fit(X, y)

    Ar = np.array([clf.predict([[features[k, i], features[k, j]]]) == y[k] for k in range(0, CountArray)])
    T = np.where(Ar == True)[0]
    P.append(np.size(T)/CountArray)

print(P)
MaxEl = np.argmax(P)
print('Наибольшая информативность:', Indexes[MaxEl])
print('Вероятность:', P[MaxEl])


