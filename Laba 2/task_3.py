from sklearn.datasets import load_digits
import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
import matplotlib.pyplot as plt
digits = load_digits()


Accuracy = []

X_train1, y_train1 = digits.data[0:1000], digits.target[0:1000]
X_test1, y_test1 = digits.data[1000:1500], digits.target[1000:1500]

clf1 = GaussianNB()
clf1.fit(X_train1, y_train1)

y_result1 = clf1.predict(digits.data[1000:1500])

T1 = np.where(y_result1 == y_test1)[0]
F1 = np.where(y_result1 != y_test1)
print('Наиболее частая ошибка:', np.bincount(y_result1[F1]).argmax())
Accuracy.append(len(T1)/500)
print('Точность:', Accuracy[0])


X_train2, y_train2 = digits.data[0:1000], digits.target[0:1000]
X_test2, y_test2 = digits.data[1000:1500], digits.target[1000:1500]

clf2 = LogisticRegression()
clf2.fit(X_train2, y_train2)

y_result2 = clf2.predict(digits.data[1000:1500])

T2 = np.where(y_result2 == y_test2)[0]
F2 = np.where(y_result2 != y_test2)
print('Наиболее частая ошибка:', np.bincount(y_result2[F2]).argmax())
Accuracy.append(len(T2)/500)
print('Точность:', Accuracy[1])

X_train3, y_train3 = digits.data[0:1000], digits.target[0:1000]
X_test3, y_test3 = digits.data[1000:1500], digits.target[1000:1500]

clf3 = RandomForestClassifier(max_depth=2, random_state=0)
clf3.fit(X_train3, y_train3)

y_result3 = clf3.predict(digits.data[1000:1500])

T3 = np.where(y_result3 == y_test3)[0]
F3 = np.where(y_result3 != y_test3)
print('Наиболее частая ошибка:', np.bincount(y_result3[F3]).argmax())
Accuracy.append(len(T3)/500)
print('Точность:', Accuracy[2])

print('Наибольшая производительность: LogisticRegression')

N = set(np.array(F1)[0]) & set(np.array(F2)[0]) & set(np.array(F3)[0])
print('Самые сложные изображения:', np.array(list(N)))
N = np.array(list(N))
plt.gray()
plt.matshow(digits.images[N[2]])
plt.show()


